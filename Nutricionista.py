import json


class Nutricionista:
    def __init__(self, archivo_json):
        with open(archivo_json, "r") as file:
            self.data = json.load(file)

    def obtener_nutrientes_alimento(self, nombre_alimento):
        for alimento_data in self.data["alimentos"]:
            if alimento_data["nombre"].lower() == nombre_alimento.lower():
                nutrientes_data = alimento_data["nutrientes"]
                return nutrientes_data
        return None

    def agregar_alimento(self, nuevo_alimento):
        self.data["alimentos"].append(nuevo_alimento)
        with open("nutricion.json", "w") as file:
            json.dump(self.data, file, indent=4)

    def existe_alimento(self, nombre_alimento):
        for alimento in self.data["alimentos"]:
            if alimento["nombre"].lower() == nombre_alimento.lower():
                return True
        return False

    def consultar_alimentos(self, dia, comida=None):
        dieta = self.data["dietas"][0]["menu_semanal"]
        if dia in dieta:
            alimentos_del_dia = dieta[dia]
            alimentos_por_comida = {"desayuno": [], "comida": [], "cena": []}
            for tipo_comida, alimentos in alimentos_del_dia.items():
                alimentos_por_comida[tipo_comida] = alimentos
            if comida:
                return {comida: alimentos_por_comida[comida]}
            else:
                return alimentos_por_comida

        return {"desayuno": [], "comida": [], "cena": []}

    def agregar_alimento_a_comida(self, dia, tipo_comida, nombre_alimento):
        # Convertir día y tipo de comida a minúsculas para consistencia
        dia = dia.lower()
        tipo_comida = tipo_comida.lower()

        # Verificar si el día es válido
        if dia not in self.data["dietas"][0]["menu_semanal"]:
            raise ValueError(
                "Día inválido. Debe ser uno de: lunes, martes, miércoles, jueves, viernes, sábado, domingo."
            )

        # Verificar si el tipo de comida es válido
        if tipo_comida not in ["desayuno", "comida", "cena"]:
            raise ValueError(
                "Tipo de comida inválido. Debe ser unow de: desayuno, comida, cena."
            )

            # Verificar si el alimento existe
        if not self.existe_alimento(nombre_alimento):
            raise ValueError(
                f"El alimento '{nombre_alimento}' no está en la lista de alimentos."
            )

        # Obtener el menú del día
        menu_del_dia = self.data["dietas"][0]["menu_semanal"][dia]

        # Agregar el alimento al tipo de comida correspondiente
        if tipo_comida not in menu_del_dia:
            menu_del_dia[tipo_comida] = []
        menu_del_dia[tipo_comida].append(nombre_alimento)

        # Guardar los cambios en el archivo JSON
        with open("nutricion.json", "w") as file:
            json.dump(self.data, file, indent=4)

        print(f"Alimento '{nombre_alimento}' agregado a {tipo_comida} del {dia}.")
