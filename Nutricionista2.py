import xml.etree.ElementTree as ET


class Nutricionista2:
    def __init__(self, archivo_xml):
        self.tree = ET.parse(archivo_xml)
        self.root = self.tree.getroot()

    def obtener_nutrientes_alimento(self, nombre_alimento):
        for alimento_data in self.root.findall(".//alimento"):
            if alimento_data.find("nombre").text.lower() == nombre_alimento.lower():
                nutrientes_data = alimento_data.find("nutrientes")
                nutrientes = {}
                for nutriente in nutrientes_data:
                    nutrientes[nutriente.tag] = nutriente.text
                return nutrientes
        return None

    def agregar_alimento(self, nuevo_alimento):
        alimentos = self.root.find(".//alimentos")
        alimentos.append(nuevo_alimento)
        self.tree.write("nutricion.xml")

    def consultar_alimentos(self, dia, comida=None):
        print("Consultando alimentos para el día:", dia)
        dieta = self.root.find(".//dietas/dieta[nombre='Dieta Semanal']/menu_semanal")
        if dieta is not None:
            print("Dieta encontrada:", ET.tostring(dieta))
            dia_elemento = dieta.find(dia)
            if dia_elemento is not None:
                print("Alimentos del día encontrados:", ET.tostring(dia_elemento))
                alimentos_por_comida = {"desayuno": [], "comida": [], "cena": []}
                for tipo_comida in alimentos_por_comida:
                    alimentos = dia_elemento.find(tipo_comida)
                    if alimentos is not None:
                        print(
                            "Alimentos para",
                            tipo_comida,
                            "encontrados:",
                            ET.tostring(alimentos),
                        )
                        alimentos_por_comida[tipo_comida] = [
                            alimento.text for alimento in alimentos.findall("alimento")
                        ]
                if comida:
                    return {comida: alimentos_por_comida[comida]}
                else:
                    return alimentos_por_comida
        print("No se encontraron datos para el día", dia)
        return {"desayuno": [], "comida": [], "cena": []}
