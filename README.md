# Práctica PER24-pfinal-Nutricion🥗
![ CLICK AQUI PARA DEPLOY DEL PROYECTO (MVP)](http://waldemar.pythonanywhere.com/)`_Deploy Proyecto, Versión Mínima, para versión ampliada clonar este repositorio y probar servidor en local._`

![Demostración del Proyecto](https://s12.gifyu.com/images/SfRM5.gif)

Deploy versión movil (ver mediaQUERY)

![Demostración del Proyecto](https://s10.gifyu.com/images/SfRLM.gif)

Importante a tener en cuenta que tiene desarrollos en JS y detalles como carga personalizada al consultar y añadir un alimento por formulario y giro de formulario clickeando en el emoticono 🥗 que lleva de un formulario a otro con un evento de giro JS, también he tenido que jugar con la caché en JS para poder solucionar el problema de volver y que el loader no siguiera cargando indefinidamente formandose un fallo.

*Importante tener en cuenta que aunque hay varios alimentos ya metidos en los ficheros json con el mismo identificador (nombre) se ha limitado que no se puedan añadir alimentos con el mismo nombre, a través de:

`
        def existe_alimento(self, nombre_alimento):
            for alimento in self.data["alimentos"]:
                if alimento["nombre"].lower() == nombre_alimento.lower():
                    return True
            return False
`

El objetivo principal de este proyecto es desarrollar un servidor web en Python que permita gestionar dietas, alimentos y nutrientes. Además del servidor web, el proyecto incluye una batería de tests unitarios.

**Características Principales**

- `Manejo de peticiones HTTP:` El foco principal del proyecto está en el manejo de peticiones GET y POST en un servidor HTTP, permitiendo consultar y agregar alimentos, así como manejar sus nutrientes.

- `Tratamiento de excepciones:`Implementación de un robusto tratamiento de excepciones para asegurar la fiabilidad del servidor.

- `Programación Orientada a Objetos (POO):` Uso de POO para estructurar el código de manera modular y reutilizable.

- `Gestión de datos:` Acceso y manipulación de datos almacenados en ficheros JSON y XML, permitiendo consultar información de alimentos y sus nutrientes desde diferentes fuentes.

- `Integración con APIs externas:` Obtención de datos nutricionales de una API externa para complementar la información almacenada localmente.

- `Desarrollo frontend:` Uso de HTML y JavaScript para crear formularios interactivos y presentar los datos de manera atractiva y funcional.

- `Despliegue en PythonAnywhere:` El servidor ha sido desplegado en PythonAnywhere, permitiendo su acceso y uso a través de la web.


**Funcionalidades**

- **Consultas de alimentos:** Consultar alimentos y sus nutrientes por día de la semana y tipo de comida (desayuno, comida, cena).

- **Manejo de archivos estáticos:** Servir archivos estáticos como hojas de estilo (CSS) e imágenes.

- **Manejo de formularios:** Formularios en HTML y funcionalidad con JavaScript para la consulta y adición de alimentos y nutrientes.

- **Soporte para múltiples formatos:** Lectura de datos tanto desde archivos JSON como XML.


## Estructura del proyecto 📁

El proyecto tiene 19 recursos, entre los que se incluyen:

- **Clases:**
  - `Nutricionista.py`: Provee métodos para trabajar con ficheros JSON. Se encarga de cargar e introducir datos en formato JSON.
  - `Nutricionista2.py`: Provee métodos para trabajar con ficheros XML. Hace exactamente las mismas funciones que `Nutricionista.py` pero sobre ficheros XML, permitiendo elegir entre trabajar con JSON o XML.

- **HTML:**
  - `autor.html`: Página del autor del proyecto.
  - `buscador.html`: Recurso con un input, lógica JS y CSS para buscar los nutrientes de un alimento llamando a la API.
  - `formulario.html`: Formulario principal sobre el que se construye todo el proyecto.
  - `formulario5.html`: Formulario para ligar un alimento ya creado a una semana y comida.

- **Imágenes:**
  - `logoURJC.png`: Logo modificado de la URJC que lleva a la página del autor.

- **JSON:**
  - `n_test.json`: Archivo JSON utilizado únicamente para testear los métodos JSON.
  - `nutricion.json`: Archivo JSON principal sobre el que trabaja el servidor web.

- **XML:**
  - `nutricion.xml`: Otro archivo principal sobre el que trabaja la aplicación.

- **JavaScript:**
  - `script.js`: Gestiona los tres eventos principales del servidor: permitir mostrar dos formularios en la misma página a través de un evento de giro, y la transición de carga cuando se solicitan y traen datos, aplicándose a cada formulario.

- **CSS:**
  - `estiloautor.css`: Estilos de la página del autor.
  - `styles.css`: Estilos utilizados en diferentes recursos CSS.

- **Servidores:**
  - `servidor.py`: Contiene toda la lógica del servidor HTTP.
  - `servidorflask.py`: Servidor principal en MVP desplegado en PythonAnywhere.

- **Tests:**
  - `test_servidor.py`: Tests para peticiones GET y manejo de excepciones.
  - `test_servidor.json`: Tests para métodos relacionados con JSON.
  - `test_servidor_post.py`: Tests unitarios para peticiones POST.

- **Iconos:**
  - `favicon.ico`: Icono utilizado para evitar errores de petición por falta de este recurso que el servidor solicita indirectamente.


## Cuestiones ❓

**Estructura de una URL y las URL utilizadas en la práctica**

Esquema (Scheme):
    Indica el protocolo que se debe usar para acceder al recurso.
    Ejemplos: http, https, ftp, mailto.

Usuario:Contraseña (Userinfo) (opcional):
    Incluye la información de autenticación.
    Formato: usuario:contraseña@
    Ejemplo: user:password@

Host:
    Especifica el dominio o la dirección IP donde reside el recurso.
    Ejemplos: www.ejemplo.com, 192.168.0.1.

Puerto (Port) (opcional):
    Especifica el puerto a través del cual se debe acceder al recurso.
    Se añade después del host, separado por dos puntos :.
    Ejemplo: :8080

Ruta (Path):
    Indica la ubicación específica del recurso en el servidor.
    Ejemplos: /path/to/resource, /images/picture.jpg.

Consulta (Query) (opcional):
    Proporciona parámetros adicionales que pueden ser utilizados por el servidor para filtrar o modificar la respuesta.
    Se añade después de la ruta, precedida por un signo de interrogación ?.
    Ejemplo: ?key1=value1&key2=value2.

Fragmento (Fragment) (opcional):
    Indica una sección específica del recurso.
    Se añade al final de la URL, precedido por un signo de almohadilla #.
    Ejemplo: #section2

Ejemplo de una URL completa

https://user:password@www.ejemplo.com:8080/path/to/resource?key1=value1&key2=value2#section2

Desglose del ejemplo

- Esquema: https
- Usuario:Contraseña: user:password
- Host: www.ejemplo.com
- Puerto: 8080
- Ruta: /path/to/resource
- Consulta: ?key1=value1&key2=value2
- Fragmento: #section2

Explicación

- https: Es el protocolo usado para la transferencia segura de datos.
- user:password@: Proporciona información de autenticación (a menudo omitida por razones de seguridad).
- www.ejemplo.com: Es el dominio del servidor donde se encuentra el recurso.
- :8080: Es el puerto en el que el servidor está escuchando.
- /path/to/resource: Indica la ubicación exacta del recurso en el servidor.
- ?key1=value1&key2=value2: Proporciona parámetros de consulta que pueden afectar cómo se procesa o devuelve el recurso.
- #section2: Indica una sección específica dentro del recurso.




Ejemplo de una URL completa en mi proyecto.

`http://localhost:21500/consulta?dia_semana=lunes&tipo_comida=`



### Desglose
1. **Esquema (Scheme)**: `http`
2. **Host**: `localhost`
3. **Puerto (Port)**: `21500`
4. **Ruta (Path)**: `/consulta`
5. **Consulta (Query)**: `?dia_semana=lunes&tipo_comida=`

### Explicación
- **http**: Es el protocolo utilizado. En este caso, es `http` (HyperText Transfer Protocol), que es el protocolo estándar para la web. No es seguro como `https`, pero es común para desarrollo local y pruebas.
- **localhost**: Es el host, que indica que el recurso está alojado en la máquina local (tu computadora) en lugar de en un servidor remoto. `localhost` es una referencia a `127.0.0.1`, la dirección loopback estándar en la red local.

- **:21500**: Especifica el puerto a través del cual se debe acceder al recurso. En este caso, el puerto es `21500`. Esto es útil para diferenciar entre múltiples servicios que pueden estar ejecutándose en la misma máquina.

- **/consulta**: Indica la ruta específica en el servidor donde se encuentra el recurso o el punto final (endpoint) al que se está accediendo. En este caso, `/consulta` podría ser un endpoint en una aplicación web que maneja consultas.

- **?dia_semana=lunes&tipo_comida=**: Es la cadena de consulta que proporciona parámetros adicionales al servidor.
  - `dia_semana=lunes`: Es un parámetro de consulta donde `dia_semana` es la clave y `lunes` es su valor. Esto puede ser utilizado por el servidor para filtrar o procesar datos específicos para el día lunes.

  - `tipo_comida=`: Es otro parámetro de consulta donde `tipo_comida` es la clave y su valor está vacío. Esto podría ser utilizado por el servidor para realizar alguna operación relacionada con el tipo de comida, aunque en este caso al no especficiar el valor por defecto el servidor valora ese caso como devlución de todas las comidas, devolviendo las comidas de desayuno, comida y cena del del lunes.


**URL USADAS EN LA PRÁCTICA:**

`http://localhost:21500/consulta?dia_semana=lunes&tipo_comida=desayuno`

Esta URL se utiliza para obtener los alimentos del lunes y específicamente del desayuno. La consulta incluye los parámetros 'dia_semana' con el valor 'lunes' y 'tipo_comida' con el valor 'desayuno'. La respuesta devuelve los alimentos en una columna HTML con estilos.

Otras combinaciones válidas para esta URL son:
- 'dia_semana' puede ser: lunes, martes, miércoles, jueves, viernes, sábado, domingo.
- 'tipo_comida' puede ser: desayuno, comida, cena, None.

Cualquier otra combinación de valores provocará una respuesta de error con un status_code 404

`http://localhost:21500/XML?dia_semana=lunes&tipo_comida=desayuno`

la ruta `sería /XML`

y la query `?dia_semana=lunes&tipo_comida"desayuno`

es lo mismo que en la consulta anterior, lo único que consultado a un fichero XML.

`http://localhost:21500/autor`

en esta url el path es /autor y no habría query ya que no hay que condicionar esa consulta o filtrarla.

`http://localhost:21500/consulta/v2/alimento`

la ruta o el path sería /consulta/v2/alimento tampoco tiene query y es donde se realiza la llamada get para pedir un alimento a la api siendo la palabra alimento la que según como la cambies devolverá un alimento ú otro que encuentre en la api

URL: `http://localhost:21500/alimento/pera`

Descripción:

- Esquema (Scheme): http
- Host: localhost
- Puerto (Port): 21500
- Ruta (Path): /alimento/pera
- Consulta (Query): (no hay consulta en esta URL)
- Fragmento (Fragment): (no hay fragmento en esta URL)

Esta URL apunta al recurso /alimento/pera en el servidor local, donde se espera obtener el valor nutricional del alimento "pera". Si el servidor encuentra este alimento en el archivo correspondiente, devolverá sus valores nutricionales. Sin embargo, si no lo encuentra, devolverá un error 404.

URL:` http://localhost:21500/prueba`

Descripción:

- Esquema (Scheme): http
- Host: localhost
- Puerto (Port): 21500
- Ruta (Path): /prueba
- Consulta (Query): (no hay consulta en esta URL)
- Fragmento (Fragment): (no hay fragmento en esta URL)

Esta URL apunta al recurso /prueba en el servidor local. Es un recurso con un formulario que permite agregar un alimento ya creado a un día específico de la semana en la dieta, así como especificar la comida (desayuno, comida o cena).



**Diferencia entre HTML y JSON**


_HTML (HyperText Markup Language)_

- HTML es el lenguaje estándar utilizado para crear páginas web.

- Se utiliza para estructurar y presentar contenido en la web, como texto, imágenes, videos, formularios, etc.

- HTML utiliza etiquetas para definir la estructura y el formato del contenido.

- HTML es como el esqueleto fundamental de un programa, especialmente en lo que respecta al diseño y la presentación en pantalla. A partir de este esqueleto, podemos aplicar estilos o acceder a sus identificadores (IDs) para interactuar con los elementos en la pantalla.

- Es un lenguaje de marcado que define la jerarquía y la relación entre los diferentes elementos de una página web.
- Ejemplo de HTML:
  ```html
  <html>
    <head>
      <title>Ejemplo de HTML</title>
    </head>
    <body>
      <h1>¡Hola, mundo!</h1>
      <p>Este es un ejemplo de HTML.</p>
    </body>
  </html>



**JSON (JavaScript Object Notation)**

JSON es un formato ligero de intercambio de datos.
Se utiliza principalmente para transmitir datos entre un servidor y un navegador web, aunque también es utilizado en muchos otros contextos.
JSON está basado en JavaScript, pero es independiente de cualquier lenguaje de programación.
Se utiliza para representar datos estructurados como objetos y arrays.
Es fácil de leer y escribir para humanos, y fácil de parsear y generar para máquinas.

Ejemplo de JSON:

    {
      "nombre": "Juan",
      "edad": 30,
      "ciudad": "Ciudad de México"
    }


Por lo que uno se suele usar para estructura y presentación web el otro más para el intercambio de datos entre sistemas.

Cuando se utiliza HTML para contener datos, estos a menudo están incrustados dentro de las etiquetas de presentación y pueden ser más difíciles de extraer y procesar en comparación con el formato claro y estructurado proporcionado por JSON. Además, HTML no proporciona las mismas herramientas y métodos para manipular y procesar datos que JSON, que está diseñado específicamente para ese propósito. Lo que es más engorroso html para contener datos que JSON que es de rápido entendimiento y procesamiento.

Cabeceras HTTP. Qué cabeceras se utilizan en la práctica

**Tipos de Cabeceras**

Hay diferentes tipos de cabeceras HTTP, como:

- **Cabeceras de Solicitud**: Enviadas por el cliente al servidor para solicitar recursos. Se utiliza en la práctica 
- **Cabeceras de Respuesta**: Enviadas por el servidor al cliente en respuesta a una solicitud. Se utiliza en la práctica
- **Cabeceras Generales**: Aplicables tanto a las solicitudes como a las respuestas.
- **Cabeceras de Entidad**: Relacionadas con el cuerpo del mensaje (contenido).

**Funcionalidades Comunes**

- **Content-Type**: Especifica el tipo de medio del contenido del recurso (por ejemplo, text/html para HTML, application/json para JSON, etc.).
- **Content-Length**: Indica la longitud del contenido en bytes.
- **Cache-Control**: Controla cómo se almacena en caché una respuesta tanto en el navegador como en servidores intermedios.
- **User-Agent**: Identifica el software del cliente que realiza la solicitud.
- **Set-Cookie**: Establece una cookie en el navegador del cliente.
- **Location**: Utilizada en respuestas de redireccionamiento para indicar la nueva ubicación de un recurso.

**Manipulación de Cabeceras**

- En el lado del servidor, las cabeceras HTTP pueden ser manipuladas mediante el código del servidor para personalizar la respuesta a una solicitud.
- En el lado del cliente, algunas cabeceras pueden ser manipuladas a través de scripts en el navegador, como JavaScript, para realizar acciones como enviar solicitudes Ajax o manejar cookies.

**Seguridad y Privacidad**

Algunas cabeceras están relacionadas con la seguridad y la privacidad, como:

- **HTTP Strict Transport Security (HSTS)**: Indica que el navegador debe acceder al sitio solo a través de HTTPS.
- **Cross-Origin Resource Sharing (CORS)**: Permite que recursos de un origen tengan permiso para ser utilizados por un dominio diferente.

**Ejemplos de Uso**




**Cabeceras HTTP. Qué cabeceras se utilizan en la práctica**

En el contexto del servidor ServidorNutricion, algunas de las cabeceras HTTP utilizadas son:

    Content-type: Indica el tipo de contenido de la respuesta. Por ejemplo, text/html; charset=utf-8 indica que el contenido es HTML con codificación UTF-8.
    Content-Length: Indica la longitud del cuerpo de la solicitud o respuesta, en bytes.
    Status: Indica el código de estado de la respuesta, como 200 OK para una solicitud exitosa o 404 Not Found para un recurso no encontrado.

Referencias en código:

La cabecera de la respuesta HTTP incluye información sobre el tipo de contenido, el estado de la respuesta, y otras metadatos.
    Se configura usando métodos como self.send_response y self.send_header dentro de la función self.set_response.
    Después de configurar todas las cabeceras necesarias, self.end_headers es llamado para finalizar la sección de cabeceras.

Contenido:

El contenido o cuerpo de la respuesta es lo que realmente se envía al cliente como el HTML, texto, JSON, etc.
    Se envía usando self.wfile.write.

Método set_response() para establecer la respuesta http según el cdigo y contenido que se de y por defecto asigna html y status_code = 200.

    def set_response(self, status_code=200, content_type="text/html; charset=utf-8"):
        """Establece la respuesta HTTP.

        Args:
            status_code (int): Código de estado HTTP. Default es 200.
            content_type (str): Tipo de contenido de la respuesta. Default es "text/html; charset=utf-8".
        """
        self.send_response(status_code)
        self.send_header("Content-type", content_type)
        self.end_headers()



Y uso de esté metodo en peticiones GET

```
    def do_GET(self):
        ...
        if self.path == "/logoURJC.png":
            self.handle_logo_request()
        ...
        elif self.path.startswith("/consulta/v2"):
            self.handle_alimento_request()
        ...
        elif self.path == "/autor":
            self.set_response(status_code=200)
            self.wfile.write(self.pagina_autor().encode("utf-8"))

    def handle_logo_request(self):
        """Maneja la solicitud para el archivo de imagen del logo."""
        try:
            self.set_response(status_code=200, content_type="image/png")
            with open("logoURJC.png", "rb") as f:
                self.wfile.write(f.read())
        except FileNotFoundError as e:
            self.send_error(404, "Archivo del logo no encontrado: {}".format(str(e)))

    def handle_styles_request(self):
        """Maneja la solicitud para el archivo de estilos."""
        try:
            self.set_response(status_code=200, content_type="text/css; charset=utf-8")
            with open("styles.css", "rb") as f:
                self.wfile.write(f.read())
        except FileNotFoundError as e:
            self.send_error(404, "Archivo de estilos no encontrado: {}".format(str(e)))
```

    def handle_index_request(self):
        """Maneja la solicitud para el archivo de estilos."""
        self.set_response(status_code=200)
        self.wfile.write(self.form().encode("utf-8"))
        self.wfile.write(self.elemento_buscador().encode("utf-8"))

    def handle_favicon_request(self):
        """Maneja la solicitud para favicon.ico."""
        try:
            with open("favicon.ico", "rb") as file:
                self.set_response(status_code=200, content_type="image/x-icon")
        except FileNotFoundError:
            self.send_error(404, "Favicon no encontrado")

POST CABECERAS

      def do_POST(self):
          ...
          if self.path == "/agregar_alimento":
              ...
              nutricionista = Nutricionista("nutricion.json")
              ...
              if nutricionista.existe_alimento(nombre_alimento):
                  self.set_response(status_code=400)
                  self.wfile.write("Ya existe este alimento".encode("utf-8"))
                  return
              ...
              nutricionista.agregar_alimento(nuevo_alimento2)
              self.set_response()
              self.wfile.write(
                  "<p class='bb'>Nuevo alimento agregado: {}</p>".format(
                      nuevo_alimento2
                  ).encode("utf-8")
              )
          ...

ENVÍO DE ERRORES

    except FileNotFoundError as e:
        self.send_error(404, "Archivo no encontrado: {}".format(str(e)))

    python

    except ValueError as e:
        self.send_error(400, "Solicitud malformada: {}".format(str(e)))

    python

    except Exception as e:
        self.send_error(500, "Error interno del servidor en la solicitud GET: {}"format(str(e)))




# ¿Qué es una API REST?

Es una lógica de restricciones y recomendaciones bajo las cuales se puede construir una API no es un framework ni un protocolo ni está limitado a un lenguaje de programación.

_"Una API REST (Representational State Transfer) es un conjunto de reglas y convenciones para diseñar y desarrollar servicios web que permiten la comunicación entre sistemas de software a través del protocolo HTTP. Este enfoque arquitectónico se basa en los principios de la web y se centra en los recursos y sus representaciones."_

## Características principales

- **Basada en recursos**: Los recursos son entidades de información, como objetos o datos, que pueden ser accedidos o manipulados a través de la API. Cada recurso tiene un identificador único (URL) y está representado en diferentes formatos, como JSON o XML.

- **Operaciones estándar de HTTP**: Utiliza los métodos HTTP estándar (GET, POST, PUT, DELETE, etc.) para realizar operaciones sobre los recursos. Por ejemplo, GET se utiliza para recuperar recursos, POST para crear nuevos recursos, PUT para actualizar recursos existentes y DELETE para eliminar recursos.

- **Sin estado**: Las solicitudes HTTP deben ser autocontenidas y no deben depender del estado del servidor. Esto significa que cada solicitud debe contener toda la información necesaria para que el servidor comprenda y procese la solicitud, sin necesidad de almacenar estado del cliente en el servidor entre solicitudes.

- **Interfaz uniforme**: Utiliza una interfaz uniforme para acceder y manipular recursos, lo que facilita la interoperabilidad entre diferentes sistemas. Esta interfaz incluye la identificación de recursos mediante URLs, el uso de métodos HTTP estándar y el uso de tipos de contenido para representar los recursos.

- **Uso de hipermedia (HATEOAS)**: Las respuestas de la API pueden incluir enlaces a otros recursos relacionados, lo que permite a los clientes navegar y descubrir la funcionalidad de la API de forma dinámica. Esto promueve la desacopladura entre el cliente y el servidor, ya que el cliente puede interactuar con la API sin necesidad de conocer todas las URL de antemano.

**Ejemplos de API REST:**

API de Twitter: Permite a los desarrolladores acceder y manipular tweets, usuarios, listas, y más a través de HTTP utilizando métodos como GET, POST, PUT y DELETE.

API de GitHub: Proporciona acceso a repositorios, usuarios, organizaciones, y más, permitiendo a los desarrolladores integrar la funcionalidad de GitHub en sus propias aplicaciones.

API de Google Maps: Permite a los desarrolladores integrar mapas interactivos, imágenes satelitales, y datos de geolocalización en sus aplicaciones web o móviles.

Lo bueno de APIREST es que funcionan como cajas negras el servidor y el cliente y cada uno puede tener su implementación y su propio lenguaje. 

**Ejemplo de API REST en el proyecto:**

AEl método `obtener_nutrientes_api()` actúa como un cliente para una API REST externa. Esta función realiza una solicitud GET a la API de alimentos del USDA para obtener los nutrientes de un alimento específico. 


        def obtener_nutrientes_api(self, alimento):
            """Obtiene los nutrientes de un alimento desde una API externa."""
            api_key = "Zs44GKvS6fABA2Os35YgzK9mxWtx60dB3iITuDfb"
            url = "https://api.nal.usda.gov/fdc/v1/foods/list"
            params = {"api_key": api_key, "query": alimento}
            try:
                response = requests.get(url, params)

                if response.status_code == 200:
                    data = response.json()

                    if data:
                        primer_alimento = data[0]
                        nutrientes = {
                            "proteinas": self.get_nutriente(primer_alimento, "Protein"),
                            "carbohidratos": self.get_nutriente(primer_alimento, "Carbohydrate, by difference"),
                            "grasas": self.get_nutriente(primer_alimento, "Total lipid (fat)"),
                            "fibra": self.get_nutriente(primer_alimento, "Fiber, total dietary"),
                            "colesterol": self.get_nutriente(primer_alimento, "Cholesterol"),
                        }
                        return nutrientes
                    else:
                        print("No se encontraron valores nutricionales para el alimento en la API.")
                        return None
            except Exception as e:
                print("Error al obtener los valores nutricionales de la API:", e)
                return None


**Verbos HTTP. Qué es un verbo HTTP y qué verbos se utilizan en la práctica y dónde.**

Verbos HTTP
¿Qué es un Verbo HTTP?

Un verbo HTTP, también conocido como método HTTP, es una acción que especifica el tipo de operación que se desea realizar en un recurso en el servidor a través de una solicitud HTTP. Los verbos HTTP son esenciales en la comunicación entre los clientes (como navegadores web o aplicaciones) y los servidores, determinando cómo se deben manejar las solicitudes.
Principales Verbos HTTP y sus Usos
1. GET

    Descripción: Solicita la representación de un recurso específico. Las solicitudes GET deben ser seguras y no deben modificar el estado del servidor.
    Uso Común: Recuperar datos de un servidor, como páginas web, imágenes, o datos de una API.
    Ejemplo:


    response = requests.get('https://api.example.com/data')

2. POST

    Descripción: Envía datos al servidor para crear un nuevo recurso. Puede modificar el estado del servidor.
    Uso Común: Enviar datos a un servidor, como formularios de registro, subir archivos, o añadir nuevos registros en una base de datos.
    Ejemplo:


    response = requests.post('https://api.example.com/data', json={"name": "John", "age": 30})

3. PUT

    Descripción: Actualiza un recurso existente o crea uno nuevo si no existe. Las solicitudes PUT son idempotentes, lo que significa que repetir la misma solicitud tendrá el mismo efecto.
    Uso Común: Actualizar registros completos en un servidor.
    Ejemplo:


    response = requests.put('https://api.example.com/data/1', json={"name": "John", "age": 31})

4. PATCH

    Descripción: Aplica modificaciones parciales a un recurso. A diferencia de PUT, que reemplaza todo el recurso, PATCH solo modifica las partes especificadas.
    Uso Común: Actualizar campos específicos de un registro.
    Ejemplo:

    

    response = requests.patch('https://api.example.com/data/1', json={"age": 31})

5. DELETE

    Descripción: Elimina un recurso específico del servidor.
    Uso Común: Borrar registros de una base de datos o eliminar archivos del servidor.
    Ejemplo:


    response = requests.delete('https://api.example.com/data/1')

6. HEAD

    Descripción: Similar a GET, pero solo solicita los encabezados de la respuesta, sin el cuerpo del contenido. Utilizado principalmente para verificar qué datos devuelve una solicitud GET sin descargar el cuerpo completo.
    Uso Común: Comprobar la existencia de un recurso o meta-información sobre él.
    Ejemplo:


    response = requests.head('https://api.example.com/data')

7. OPTIONS

    Descripción: Describe las opciones de comunicación para el recurso de destino. Permite saber qué métodos HTTP están soportados por el servidor para un recurso específico.
    Uso Común: Verificar las capacidades del servidor o configurar la comunicación.
    Ejemplo:


response = requests.options('https://api.example.com/data')

## Construido con 🛠️

**Python:** Lenguaje de programación principal utilizado para el desarrollo del servidor web.

**http.server:** Módulo estándar de Python utilizado para crear el servidor HTTP.

**Flask:** Micro framework web utilizado para manejar rutas y solicitudes HTTP.

**HTML:** Lenguaje de marcado utilizado para la estructura y contenido de las páginas web.

**CSS:** Lenguaje de hojas de estilo utilizado para la presentación y diseño de las páginas web.

**JavaScript:** Lenguaje de programación utilizado para la interactividad en el lado del cliente.

**JSON:** Formato de datos utilizado para almacenar y manejar información sobre alimentos y nutrientes.

**XML:** Formato de datos alternativo utilizado para almacenar y manejar información sobre alimentos y nutrientes.

**requests:** Biblioteca de Python utilizada para realizar solicitudes HTTP a APIs externas.

**PythonAnywhere:** Plataforma de alojamiento utilizada para desplegar el servidor web.

## Contribuyendo 🖇️
 A la espera de compañeros que se sumen.

## Versionado 📌

Versión 2.0

## Curiosidades / Recursos extras 📌

He descubierto multitud de páginas que sirven para hacer deploys de aplicaciones que necesitan lanzarservidores, lo cual no sabía ya que he trabajado con aplicaciones estáticas de frontend anteriormente y he encontrado buenas herramientas como FireBase, Heroku, Pythonanywhere..

**He usado en el deploy media querys en CSS ajustándolas a un dispositivo movil, para poder usarse la aplicación en teléfono movil y en escritorio.**

Para las cabeceras, he usado en una ocasión dos wfile.write para enviar el contenido de una cabecera y lo ha enviado porque funciona secuencialmente wfile.write pero si quiero cambiar de cabecera por ejemplo he aprendido que hay que usar un espacio en blanco entra ambas cabeceras.

ejemplo: 
    def do_GET(self):
        # Establecer las cabeceras
        self.send_response(200)
        self.send_header("Content-type", "text/html; charset=utf-8")
        self.end_headers()

        # Escribir el primer bloque de contenido (HTML)
        self.wfile.write(b"<html><body><h1>Hello, world!</h1></body></html>")

        # Escribir una línea en blanco para separar el contenido
        self.wfile.write(b"\n")

        # Establecer las cabeceras para la imagen
        self.send_header("Content-type", "image/png")
        self.end_headers()

        # Escribir el segundo bloque de contenido (imagen)
        with open("imagen.png", "rb") as f:
            self.wfile.write(f.read())


## Autores ✒️


* **Waldemar Stegierski** - *Estudiante* - [linkedin](https://www.linkedin.com/in/waldemar-stegierski-2a6589209/)
  

## Licencia 📄

Este proyecto está bajo la Licencia MIT. 


## Expresiones de Gratitud 🎁

*Agradecimientos a  📢
* Rubén, Álvaro y Agustin por resolver en cualquier momento las dudas relacionadas con la asignatura.

## Aspectos a mejorar en el diseño 📌

Podría haber mejorado el proyecto utilizando un diseño basado en el modelo-vista-controlador (MVC). Al crecer el proyecto sin un diseño previo, he mezclado JavaScript, HTML y lógica de negocio, lo cual ha dificultado la escalabilidad, el mantenimiento y la reutilización del código.

También endos pruebas que añado alimentos si están repetidos no te deja la aplicación y envía un error, eso ha generado que no pueda añadir alimentos repetidos pero también que la prueba solo pueda ejecutarla una vez, por lo que debería investigar como asignar un alimento y borrarlo en la misma prueba para que no suceda error en las pruebas automáticas que las lanzan cada commit y generan por tanto alimentos repetidos y lanzan errores en los test.



---