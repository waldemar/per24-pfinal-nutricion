function mostrarCarga(formId) {
    document.getElementById("loader").style.display = "block";
    setTimeout(function() {
        // Envía el formulario después del retraso
        document.getElementById(formId).submit(); 
    }, 1000); // Envía el formulario después de 3 segundos

    // Retorna false para evitar que el formulario se envíe automáticamente
    return false;
}

function mostrarCarga2(formId) {
    document.getElementById("loader2").style.display = "block";
    setTimeout(function() {
        // Envía el formulario después del retraso
        document.getElementById(formId).submit(); 
    }, 1000); // Envía el formulario después de 3 segundos

    // Retorna false para evitar que el formulario se envíe automáticamente
    return false;
}

function girarFormulario() {
    var form = document.querySelector('.form-register');
    form.classList.toggle('rotated');
    
    // Cambia los campos del formulario original después de la animación de giro
    setTimeout(function() {
        if (form.classList.contains('rotated')) {
            // Muestra el segundo formulario
            document.getElementById('formOriginal').style.display = 'none';
            document.getElementById('formAdicional').style.display = 'block';
            // Cambia el texto del título del formulario
            document.querySelector('#formAdicional h4').textContent = 'Formulario Adicional 🥗';
        } else {
            // Muestra el primer formulario
            document.getElementById('formOriginal').style.display = 'block';
            document.getElementById('formAdicional').style.display = 'none';
            // Cambia el texto del título del formulario
            document.querySelector('#formOriginal h4').textContent = 'Formulario Registro 🥗';
        }
    }, 1000); // Espera 1 segundo para la animación de giro
}

// Detectar cuándo la página se muestra nuevamente desde el caché
window.addEventListener('pageshow', function(event) {
    // Si la página se muestra nuevamente desde el caché, ocultar el indicador de carga
    document.getElementById("loader").style.display = "none";
    document.getElementById("loader2").style.display = "none";
});
