from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
from Nutricionista import Nutricionista
from Nutricionista2 import Nutricionista2
import requests
import datetime


class ServerConfig:
    """Configuración del servidor."""

    HOST_ADDRESS = "127.0.0.1"
    HOST_PORT = 21500


class ServidorNutricion(BaseHTTPRequestHandler):
    """Manejador de solicitudes HTTP."""

    def set_response(self, status_code=200, content_type="text/html; charset=utf-8"):
        """Establece la respuesta HTTP.

        Args:
            status_code (int): Código de estado HTTP. Default es 200.
            content_type (str): Tipo de contenido de la respuesta. Default es "text/html; charset=utf-8".
        """
        self.send_response(status_code)
        self.send_header("Content-type", content_type)
        self.end_headers()

    def do_GET(self):
        """Maneja solicitudes GET."""
        try:
            if self.path == "/styles.css":

                self.handle_styles_request()
            elif self.path == "/favicon.ico":
                self.handle_favicon_request()
            elif self.path in [
                "/alimento/styles.css",
                "/V3/styles.css",
                "/autor/styles.css",
                "/consulta/v2/styles.css",
            ]:
                self.handle_styles_request()
            elif self.path in ["/", "/index.html", "/index.htm"]:
                self.handle_index_request()

            elif self.path == "/logoURJC.png":
                self.handle_logo_request()

            elif self.path.startswith("/consulta/v2"):
                self.handle_alimento_request()
            elif self.path.startswith("/consulta"):
                self.handle_consulta_request()
            elif self.path == "/prueba":
                self.handle_alimento_semana()
            elif self.path.startswith("/XML"):
                self.handle_XML_request()
            elif self.path.startswith("/alimento/"):
                self.handle_tablaAPIyFichero_request()

            elif self.path == "/autor":
                self.set_response(status_code=200)
                self.wfile.write(self.pagina_autor().encode("utf-8"))

            elif self.path == "/datetime":
                self.set_response(status_code=200)
                current = datetime.datetime.now()
                print("La fecha y hora actual es ésta:", current)

                self.wfile.write(self.datetime(current).encode("utf-8"))

            else:
                self.set_response(status_code=404)
                self.wfile.write("Página no encontrada.".encode("utf-8"))
        except FileNotFoundError as e:
            self.send_error(404, "Archivo no encontrado: {}".format(str(e)))
        except ValueError as e:
            self.send_error(400, "Solicitud malformada: {}".format(str(e)))

        except Exception as e:
            self.send_error(
                500, "Error interno del servidor en la solicitud GET: {}".format(str(e))
            )

    def handle_logo_request(self):
        """Maneja la solicitud para el archivo de imagen del logo."""
        try:
            self.set_response(status_code=200, content_type="image/png")
            with open("logoURJC.png", "rb") as f:
                self.wfile.write(f.read())
        except FileNotFoundError as e:
            self.send_error(404, "Archivo del logo no encontrado: {}".format(str(e)))

    def handle_styles_request(self):
        """Maneja la solicitud para el archivo de estilos."""
        try:
            self.set_response(status_code=200, content_type="text/css; charset=utf-8")
            with open("styles.css", "rb") as f:
                self.wfile.write(f.read())
        except FileNotFoundError as e:
            self.send_error(404, "Archivo de estilos no encontrado: {}".format(str(e)))

    def handle_index_request(self):
        """Maneja la solicitud para el archivo de estilos."""
        self.set_response(status_code=200)
        self.wfile.write(self.form().encode("utf-8"))
        self.wfile.write(self.elemento_buscador().encode("utf-8"))

    def handle_consulta_request(self):
        try:
            parsed_path = urlparse(self.path)
            params = parse_qs(parsed_path.query)
            day = params.get("dia_semana", [None])[0]
            food = params.get("tipo_comida", [None])[0]

            valid_days = [
                "lunes",
                "martes",
                "miércoles",
                "miercoles",
                "jueves",
                "viernes",
                "sábado",
                "sabado",
                "domingo",
            ]
            valid_foods = ["desayuno", "comida", "cena"]

            if day is None and food is None:
                self.set_response(status_code=404)
                self.wfile.write("Día y comida no especificados.".encode("utf-8"))
                return

            if day is None or day.lower() not in valid_days:
                self.set_response(status_code=404)
                self.wfile.write("Día no válido.".encode("utf-8"))
                return

            if food is not None and food.lower() not in valid_foods:
                self.set_response(status_code=404)
                self.wfile.write("Comida no válida.".encode("utf-8"))
                return

            day = day.lower()  # Convertir a minúsculas
            if food is not None:
                food = food.lower()  # Convertir a minúsculas si no es None

            self.set_response(status_code=200)
            nutricionista = Nutricionista("nutricion.json")
            alimentos = nutricionista.consultar_alimentos(day, food)
            self.wfile.write(
                self.format_alimentos_html(alimentos, food).encode("utf-8")
            )
        except FileNotFoundError as e:
            self.send_error(
                404, "Archivo de nutrición no encontrado: {}".format(str(e))
            )

    def handle_tablaAPIyFichero_request(self):
        alimento = self.path.split("/")[-1]
        nutricionista2 = Nutricionista("nutricion.json")
        nutrientes_manzana = nutricionista2.obtener_nutrientes_alimento(alimento)
        b = self.obtener_nutrientes_api(alimento)
        if nutrientes_manzana:
            self.set_response(status_code=200)
            self.wfile.write(
                self.format_nutrientes_html(nutrientes_manzana, b).encode("utf-8")
            )

        else:
            self.set_response(status_code=404)
            self.wfile.write("Alimento no encontrado.".encode("utf-8"))

    def handle_favicon_request(self):
        """Maneja la solicitud para favicon.ico."""
        try:
            with open("favicon.ico", "rb") as file:
                self.set_response(status_code=200, content_type="image/x-icon")
        except FileNotFoundError:
            self.send_error(404, "Favicon no encontrado")

    def handle_v2_request(self):
        parsed_path = urlparse(self.path)
        params = parse_qs(parsed_path.query)

        day = params.get("dia_semana", [None])[0]
        food = params.get("tipo_comida", [None])[0]
        if day is not None:
            day = day.lower()
        if food is not None:
            food = food.lower()
        self.set_response(status_code=200)
        nutricionista = Nutricionista("nutricion.json")
        alimentos = nutricionista.consultar_alimentos(day, food)
        self.wfile.write(self.format_alimentos_html(alimentos, food).encode("utf-8"))

    def handle_alimento_semana(self):
        self.set_response(status_code=200)
        self.wfile.write(self.agregar_alimento_semana().encode("utf-8"))

    def handle_XML_request(self):
        try:

            parsed_path = urlparse(self.path)
            params = parse_qs(parsed_path.query)

            day = params.get("dia_semana", [None])[0]
            food = params.get("tipo_comida", [None])[0]
            if day is not None:
                day = day.lower()
            if food is not None:
                food = food.lower()

            self.set_response(status_code=200)
            nutricionista2 = Nutricionista2("nutricion.xml")
            alimentos = nutricionista2.consultar_alimentos(day, food)
            self.wfile.write(
                self.format_alimentos_html(alimentos, food).encode("utf-8")
            )
        except ValueError as e:
            self.send_error(400, str(e))
        except FileNotFoundError as e:
            self.send_error(
                404, "Archivo XML de nutrición no encontrado: {}".format(str(e))
            )
        except Exception as e:
            self.send_error(500, "Error en la solicitud XML: {}".format(str(e)))

    def handle_alimento_request(self):
        try:
            alimento = self.path.split("/")[-1]
            nutricionista2 = Nutricionista("nutricion.json")
            nutrientes_manzana = nutricionista2.obtener_nutrientes_alimento(alimento)
            b = self.obtener_nutrientes_api(alimento)

            self.set_response(status_code=200)
            self.wfile.write(
                self.format_nutrientes_html(nutrientes_manzana, b).encode("utf-8")
            )
        except ValueError as e:
            self.send_error(404, str(e))
        except FileNotFoundError as e:
            self.send_error(
                404, "Archivo de nutrición no encontrado: {}".format(str(e))
            )
        except Exception as e:
            self.send_error(500, "Error en la solicitud de alimento: {}".format(str(e)))

    def do_POST(self):
        """Maneja las solicitudes POST al servidor.

        Este método gestiona las solicitudes POST, en particular la adición de un nuevo alimento
        a la base de datos. Extrae los datos enviados en el cuerpo de la solicitud, valida los
        nutrientes, y si todo es correcto, agrega el nuevo alimento.

        Maneja la ruta `/agregar_alimento` para agregar un nuevo alimento con sus nutrientes.
        Si la ruta solicitada no es `/agregar_alimento`, devuelve un error 404.

        Procesos clave:
        - Lee y decodifica los datos del POST.
        - Verifica que el nombre del alimento esté presente.
        - Valida y procesa los nutrientes del alimento.
        - Agrega el alimento al fichero nutricion.json

        Returns:
            None
        """
        try:
            content_length = int(self.headers["Content-Length"])
            post_data = self.rfile.read(content_length)

            if self.path == "/agregar_alimento":
                parsed_data = parse_qs(post_data.decode())

                # Verificar que el nombre del alimento está presente
                nombre_alimento = parsed_data.get("nombre_alimento", [None])[0]
                if nombre_alimento is None:
                    self.set_response(status_code=400)
                    self.wfile.write(
                        "<script>alert('Debe proporcionar el nombre del alimento.'); window.history.back();</script>".encode(
                            "utf-8"
                        )
                    )
                    return
                nutricionista = Nutricionista("nutricion.json")

                if nutricionista.existe_alimento(nombre_alimento):
                    self.set_response(status_code=400)
                    self.wfile.write("Ya existe este alimento".encode("utf-8"))
                    return

                try:
                    nuevo_alimento2 = {
                        "nombre": nombre_alimento,
                        "nutrientes": {
                            "proteinas": self.validate_nutrient(
                                parsed_data.get("proteinas", [None])[0]
                            ),
                            "carbohidratos": self.validate_nutrient(
                                parsed_data.get("carbohidratos", [None])[0]
                            ),
                            "grasas": self.validate_nutrient(
                                parsed_data.get("grasas", [None])[0]
                            ),
                            "fibra": self.validate_nutrient(
                                parsed_data.get("fibra", [None])[0]
                            ),
                            "colesterol": self.validate_nutrient(
                                parsed_data.get("colesterol", [None])[0]
                            ),
                        },
                        "categoria": parsed_data.get("categoria", [None])[0],
                    }
                except (KeyError, ValueError) as e:
                    self.set_response(status_code=400)
                    self.wfile.write(
                        "<script>alert('Debes corregir el formulario: {}'); window.history.back();</script>".format(
                            str(e)
                        ).encode(
                            "utf-8"
                        )
                    )
                    return

                nutricionista = Nutricionista("nutricion.json")
                nutricionista.agregar_alimento(nuevo_alimento2)
                self.set_response()
                self.wfile.write(
                    "<p class='bb'>Nuevo alimento agregado: {}</p>".format(
                        nuevo_alimento2
                    ).encode("utf-8")
                )

            elif self.path == "/agregar_alimento2":
                parsed_data2 = parse_qs(post_data.decode())

                nombre_alimento = parsed_data2.get("nombre_alimento", [None])[0]

                dia = parsed_data2.get("día", [None])[0]
                comida = parsed_data2.get("comida", [None])[0]
                if nombre_alimento is None:
                    self.set_response(status_code=400)
                    self.wfile.write(
                        "<script>alert('Debe proporcionar el nombre del alimento.'); window.history.back();</script>".encode(
                            "utf-8"
                        )
                    )
                    return

                nutricionista = Nutricionista("nutricion.json")

                if nutricionista.existe_alimento(nombre_alimento):

                    nutricionista.agregar_alimento_a_comida(
                        dia, comida, nombre_alimento
                    )
                    self.set_response(status_code=200)
                    self.wfile.write(
                        "<p class='bb'>Nuevo alimento agregado: {}</p>".format(
                            nombre_alimento
                        ).encode("utf-8")
                    )
                    return
                else:
                    self.set_response(status_code=400)
                    self.wfile.write("No existe este alimento".encode("utf-8"))

            else:
                self.set_response(status_code=404)
                self.wfile.write("Página no encontrada.".encode("utf-8"))
        except KeyError as e:
            self.send_error(
                400,
                "Solicitud POST malformada, falta el parámetro requerido: {}".format(
                    str(e)
                ),
            )

        except ValueError as e:
            self.send_error(
                400, "Solicitud POST malformada, valor no válido: {}".format(str(e))
            )

        except Exception as e:
            self.send_error(
                500,
                "Error interno del servidor en la solicitud POST: {}".format(str(e)),
            )

    def validate_nutrient(self, value):
        """Valida que el valor del nutriente sea un número."""
        if value is None or value.strip() == "":
            self.set_response(status_code=400)

            raise ValueError("valor nulo o cadena vacía")

        try:
            return float(value)
        except ValueError:
            raise ValueError("no es un número")

    def form(self):
        """Genera el formulario HTML para consultar alimentos por día y tipo de comida con sus estilos y js de funcionalidades.

        Returns:
            str: Código HTML del formulario.
        """
        with open("formulario.html", "r") as file:
            return file.read()

    def datetime(self, current):

        html = f"""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulario Registro</title>
    
    
</head>
<body>
    <section class="form-register">
       
            <h4 onclick="girarFormulario()">tu hora es{current}🥗</h4>
            
  </body>
    <!DOCTYPE html>
        """

        return html

    def agregar_alimento_semana(self):
        """Genera el formulario HTML para consultar alimentos por día y tipo de comida con sus estilos y js de funcionalidades.

        Returns:
            str: Código HTML del formulario.
        """
        with open("formulario5.html", "r") as file:
            return file.read()

    def elemento_buscador(self):
        """Genera el HTML y los estilos correspondiente del elemento buscador.

        Returns:
            str: Código HTML del buscador.
        """
        with open("buscador.html", "r") as file:
            return file.read()

    def pagina_autor(self):
        """Lee el contenido del autor HTML."""
        with open("autor.html", "r") as file:
            return file.read()

    def obtener_nutrientes_api(self, alimento):
        """Obtiene los nutrientes de un alimento desde una API externa."""
        api_key = "Zs44GKvS6fABA2Os35YgzK9mxWtx60dB3iITuDfb"
        url = "https://api.nal.usda.gov/fdc/v1/foods/list"
        params = {"api_key": api_key, "query": alimento}
        try:
            response = requests.get(url, params)

            if response.status_code == 200:
                data = response.json()

            if data:
                primer_alimento = data[0]
                nutrientes = {
                    "proteinas": self.get_nutriente(primer_alimento, "Protein"),
                    "carbohidratos": self.get_nutriente(
                        primer_alimento, "Carbohydrate, by difference"
                    ),
                    "grasas": self.get_nutriente(primer_alimento, "Total lipid (fat)"),
                    "fibra": self.get_nutriente(
                        primer_alimento, "Fiber, total dietary"
                    ),
                    "colesterol": self.get_nutriente(primer_alimento, "Cholesterol"),
                }
                return nutrientes
            else:
                print(
                    "No se encontraron valores nutricionales para el alimento en la API."
                )
                return None
        except Exception as e:
            print("Error al obtener los valores nutricionales de la API:", e)
            return None

    def get_nutriente(self, alimento, nombre_nutriente):
        """Obtiene la cantidad de un nutriente específico para un alimento dado.

        Args:
            alimento (dict): Diccionario que contiene la información del alimento, incluyendo una lista de nutrientes bajo la clave "foodNutrients".
            nombre_nutriente (str): El nombre del nutriente cuya cantidad se desea obtener.

        Returns:
            float or None: La cantidad del nutriente especificado si se encuentra en el alimento, o None si no se encuentra.
        """
        for nutrient in alimento.get("foodNutrients", []):
            if nutrient["name"] == nombre_nutriente:
                return nutrient["amount"]
        return None
        
    def pagina_autor(self):
        """Lee el contenido del autor HTML."""
        with open("autor.html", "r") as file:
            return file.read()

    def format_alimentos_html(self, alimentos, tipo_comida=None):
        """Genera una tabla HTML para mostrar los alimentos y sus nutrientes.

        Args:
            alimentos (list): Lista de diccionarios con la información de los alimentos.
            tipo_comida (str): Tipo de comida (desayuno, comida, cena).

        Returns:
            str: Código HTML con la tabla de alimentos.
        """
        html = "<link rel='stylesheet' type='text/css' href='styles.css'>"

        if alimentos:
            html += "<table class='container'>"
            html += "<tr>"

            comidas_disponibles = sum(
                1 for comida in ["desayuno", "comida", "cena"] if comida in alimentos
            )

            if comidas_disponibles > 1:
                html += "<th>Desayuno</th>"
                html += "<th>Comida</th>"
                html += "<th>Cena</th>"
            elif tipo_comida == "desayuno":
                html += "<th>Desayuno</th>"

            elif tipo_comida == "comida":
                html += "<th>Comida</th>"

            elif tipo_comida == "cena":
                html += "<th>Cena</th>"

            html += "</tr>"

            max_length = max(
                len(alimentos.get("desayuno", [])),
                len(alimentos.get("comida", [])),
                len(alimentos.get("cena", [])),
            )

            for i in range(max_length):
                html += "<tr>"

                if "desayuno" in alimentos:
                    alimento_desayuno = (
                        alimentos["desayuno"][i]
                        if i < len(alimentos["desayuno"])
                        else ""
                    )
                    html += f"<td>{alimento_desayuno}</td>"

                if "comida" in alimentos:
                    alimento_comida = (
                        alimentos["comida"][i] if i < len(alimentos["comida"]) else ""
                    )
                    html += f"<td>{alimento_comida}</td>"

                if "cena" in alimentos:
                    alimento_cena = (
                        alimentos["cena"][i] if i < len(alimentos["cena"]) else ""
                    )
                    html += f"<td>{alimento_cena}</td>"

                html += "</tr>"

            html += "</table>"
        else:
            html += "<p>No se encontraron alimentos para la consulta especificada.</p>"

        return html

    def format_nutrientes_html(self, nutrientes_local, nutrientes_api=None):
        """Genera una tabla HTML para mostrar los nutrientes obtenidos del fichero y la API.

        Args:
            nutrientes_fichero (dict): Diccionario con la información nutricional del fichero.
            nutrientes_api (dict): Diccionario con la información nutricional de la API.

        Returns:
            str: Código HTML con la tabla de nutrientes.
        """
        html = (
            "<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'>"
        )

        if nutrientes_local or nutrientes_api:
            html += "<div style='display:flex;'>"

            # Tabla para los valores locales
            html += "<div style='margin-right: 20px;'><p class='pp'> Fichero </p>"
            if nutrientes_local:
                html += "<table class='container'>"
                html += "<tr>"
                html += "<th>Nutriente</th>"
                html += "<th>Valor</th>"
                html += "</tr>"

                for nutriente, valor in nutrientes_local.items():
                    html += "<tr>"
                    html += f"<td>{nutriente.capitalize()}</td>"
                    html += f"<td>{valor}</td>"
                    html += "</tr>"

                html += "</table>"
            else:
                html += "<p>No se encontraron nutrientes locales para el alimento especificado.</p>"
            html += "</div>"

            # Tabla para los valores de la API
            html += "<div>"
            if nutrientes_api:
                html += "<table class='container'><p class='pp' > API </p>"
                html += "<tr>"
                html += "<th>Nutriente</th>"
                html += "<th>Valor</th>"
                html += "</tr>"

                for nutriente, valor in nutrientes_api.items():
                    html += "<tr>"
                    html += f"<td>{nutriente.capitalize()}</td>"
                    html += f"<td>{valor}</td>"
                    html += "</tr>"

                html += "</table>"
            else:
                html += (
                    "<p>No se encontraron nutrientes para el alimento en la API.</p>"
                )
            html += "</div>"

            html += "</div>"
        else:
            html += "<p>No se encontraron nutrientes para el alimento especificado.</p>"

        return html


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """Inicia el servidor HTTP.

    Args:
        server_class (HTTPServer): Clase del servidor.
        handler_class (BaseHTTPRequestHandler): Clase del manejador de solicitudes.
    """
    server_address = (ServerConfig.HOST_ADDRESS, ServerConfig.HOST_PORT)
    httpd = server_class(server_address, handler_class)
    try:
        print(
            "Servidor HTTP iniciado en {}:{}...".format(
                ServerConfig.HOST_ADDRESS, ServerConfig.HOST_PORT
            )
        )
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()
        print("Servidor HTTP detenido por el usuario.")


if __name__ == "__main__":
    run(handler_class=ServidorNutricion)
