from flask import Flask, request, send_from_directory
from urllib.parse import urlparse, parse_qs
from Nutricionista import Nutricionista
from Nutricionista2 import Nutricionista2
import requests

app = Flask(__name__)


@app.route("/")
def index():
    return form() + form2()


@app.route("/logoURJC.png")
def logo():
    return send_from_directory("", "logoURJC.png")


@app.route("/styles.css")
def styles():
    return send_from_directory("", "styles.css")


@app.route("/script.js")
def script():
    return send_from_directory("", "script.js")


@app.route("/consulta")
def consulta():
    day = request.args.get("dia_semana", "").lower()
    food = request.args.get("tipo_comida", "").lower()

    nutricionista = Nutricionista("nutricion.json")
    alimentos = nutricionista.consultar_alimentos(day, food)

    return format_alimentos_html(alimentos, food)


@app.route("/consulta/V2")
def consulta_v2():
    day = request.args.get("dia_semana", "").lower()
    food = request.args.get("tipo_comida", "").lower()

    nutricionista2 = Nutricionista2("nutricion.xml")
    alimentos = nutricionista2.consultar_alimentos(day, food)

    return format_alimentos_html(alimentos, food)


@app.route("/alimento/<alimento>")
def consulta_alimento(alimento):
    nutricionista2 = Nutricionista("nutricion.json")
    nutrientes_manzana = nutricionista2.obtener_nutrientes_alimento(alimento)
    b = obtener_nutrientes_api(alimento)

    return format_nutrientes_html(nutrientes_manzana, b)


@app.route("/autor")
def autor():
    return form3()


def form():
    with open("formulario.html", "r") as file:
        return file.read()


def form2():
    with open("formulario2.html", "r") as file:
        return file.read()


def form3():
    with open("formulario3.html", "r") as file:
        return file.read()


def obtener_nutrientes_api(alimento):
    api_key = "Zs44GKvS6fABA2Os35YgzK9mxWtx60dB3iITuDfb"
    url = "https://api.nal.usda.gov/fdc/v1/foods/list"
    params = {"api_key": api_key, "query": alimento}

    try:
        response = requests.get(url, params)

        if response.status_code == 200:
            data = response.json()

            if data:
                primer_alimento = data[0]
                nutrientes = {
                    "proteinas": get_nutriente(primer_alimento, "Protein"),
                    "carbohidratos": get_nutriente(
                        primer_alimento, "Carbohydrate, by difference"
                    ),
                    "grasas": get_nutriente(primer_alimento, "Total lipid (fat)"),
                    "fibra": get_nutriente(primer_alimento, "Fiber, total dietary"),
                    "colesterol": get_nutriente(primer_alimento, "Cholesterol"),
                }
                return nutrientes
            else:
                print(
                    "No se encontraron valores nutricionales para el alimento en la API."
                )
                return None
    except Exception as e:
        print("Error al obtener los valores nutricionales de la API:", e)
        return None


def get_nutriente(alimento, nombre_nutriente):
    for nutrient in alimento.get("foodNutrients", []):
        if nutrient["name"] == nombre_nutriente:
            return nutrient["amount"]
    return None


def format_alimentos_html(alimentos, tipo_comida=None):
    html = "<link rel='stylesheet' type='text/css' href='/styles.css'>"

    if alimentos:
        html += "<table class='container'>"
        html += "<tr>"

        comidas_disponibles = sum(
            1 for comida in ["desayuno", "comida", "cena"] if comida in alimentos
        )

        if comidas_disponibles > 1:
            html += "<th>Desayuno</th>"
            html += "<th>Comida</th>"
            html += "<th>Cena</th>"
        elif tipo_comida == "desayuno":
            html += "<th>Desayuno</th>"

        elif tipo_comida == "comida":
            html += "<th>Comida</th>"

        elif tipo_comida == "cena":
            html += "<th>Cena</th>"

        html += "</tr>"

        max_length = max(
            len(alimentos.get("desayuno", [])),
            len(alimentos.get("comida", [])),
            len(alimentos.get("cena", [])),
        )

        for i in range(max_length):
            html += "<tr>"

            if "desayuno" in alimentos:
                alimento_desayuno = (
                    alimentos["desayuno"][i] if i < len(alimentos["desayuno"]) else ""
                )
                html += f"<td>{alimento_desayuno}</td>"

            if "comida" in alimentos:
                alimento_comida = (
                    alimentos["comida"][i] if i < len(alimentos["comida"]) else ""
                )
                html += f"<td>{alimento_comida}</td>"

            if "cena" in alimentos:
                alimento_cena = (
                    alimentos["cena"][i] if i < len(alimentos["cena"]) else ""
                )
                html += f"<td>{alimento_cena}</td>"

            html += "</tr>"

        html += "</table>"
    else:
        html += "<p>No se encontraron alimentos para la consulta especificada.</p>"

    return html


def format_nutrientes_html(nutrientes_local, nutrientes_api=None):
    html = "<div class='a'> <link rel='stylesheet' type='text/css' href='/styles.css'>"

    if nutrientes_local or nutrientes_api:
        html += "<div style='display:flex;'>"

        # Tabla para los valores locales
        html += "<div style='margin-right: 20px;'><p class='pp'> Fichero </p>"
        if nutrientes_local:
            html += "<table class='container'>"
            html += "<tr>"
            html += "<th>Nutriente</th>"
            html += "<th>Valor</th>"
            html += "</tr>"

            for nutriente, valor in nutrientes_local.items():
                html += "<tr>"
                html += f"<td>{nutriente.capitalize()}</td>"
                html += f"<td>{valor}</td>"
                html += "</tr>"

            html += "</table>"
        else:
            html += "<p>No se encontraron nutrientes locales para el alimento especificado.</p>"
        html += "</div>"

        # Tabla para los valores de la API
        html += "<div>"
        if nutrientes_api:
            html += "<table class='container'><p class='pp' > API </p>"
            html += "<tr>"
            html += "<th>Nutriente</th>"
            html += "<th>Valor</th>"
            html += "</tr>"

            for nutriente, valor in nutrientes_api.items():
                html += "<tr>"
                html += f"<td>{nutriente.capitalize()}</td>"
                html += f"<td>{valor}</td>"
                html += "</tr>"

            html += "</table>"
        else:
            html += "<p>No se encontraron nutrientes para el alimento en la API.</p>"
        html += "</div>"

        html += "</div>"
    else:
        html += "<p>No se encontraron nutrientes para el alimento especificado.</p>"

    return html


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=21500)
