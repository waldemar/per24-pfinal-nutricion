import threading
import time
import unittest
import requests
from http.server import HTTPServer
from servidor import ServidorNutricion

IP = "127.0.0.1"
PORT = 21500


class TestRequests(unittest.TestCase):

    def test_autor_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/autor".format(IP, PORT))
            html = response.text
            expected_html = """
            <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cards Hover2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="styles.css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
        crossorigin="anonymous">
</head>

<body>
    <div class="container22">
        <div class="card">
            <div class="face face1">
                <div class="content">
                    <div class="icon">
                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="face face2">
                <div class="content">
                    <h3>
                        <a href="https://www.linkedin.com/in/waldemar-stegierski-2a6589209/details/education//" target="_blank">WaldemarStegierski</a>
                    </h3>
                    <p>mi linkedn</p>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="face face1">
                <div class="content">
                    <div class="icon">
                        <i class="fa fa-gitlab" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="face face2">
                <div class="content">
                    <h3>
                        <a href="https://gitlab.eif.urjc.es/waldemar/ target="_blank">Waldemar</a>
                    </h3>
                    <p>Gitlab URJC.</p>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="face face1">
                <div class="content">
                    <div class="icon">
                        <i class="fa fa-github-square" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="face face2">
                <div class="content">
                    <h3>
                        <a href="https://github.com/waaaaal/" target="_blank">Wal</a>
                    </h3>
                    <p>mi GitHub</p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
            """
            self.assertEqual(html.strip(), expected_html.strip())
            self.assertIn("<p>Gitlab URJC.</p>", response.text)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_autor_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/autor".format(IP, PORT))

            self.assertEqual(response.status_code, 200)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_alimento_error_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            nombre_alimento = "manzana"
            response = requests.get(
                "http://{}:{}/alimento/{}".format(IP, PORT, nombre_alimento)
            )
            html = response.text
            expected_html = """Alimento no encontrado."""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_alimento_error_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            nombre_alimento = "manzana"
            response = requests.get(
                "http://{}:{}/alimento/{}".format(IP, PORT, nombre_alimento)
            )

            self.assertEqual(response.status_code, 404)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_alimento_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            nombre_alimento = "apple"
            response = requests.get(
                "http://{}:{}/alimento/{}".format(IP, PORT, nombre_alimento)
            )
            html = response.text
            print(html)
            expected_html = """<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'><div style='display:flex;'><div style='margin-right: 20px;'><p class='pp'> Fichero </p><table class='container'><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.3</td></tr><tr><td>Carbohidratos</td><td>25</td></tr><tr><td>Grasas</td><td>0.4</td></tr><tr><td>Fibra</td><td>4.4</td></tr><tr><td>Colesterol</td><td>0</td></tr></table></div><div><table class='container'><p class='pp' > API </p><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.0</td></tr><tr><td>Carbohidratos</td><td>14.3</td></tr><tr><td>Grasas</td><td>0.65</td></tr><tr><td>Fibra</td><td>3.2</td></tr><tr><td>Colesterol</td><td>0.0</td></tr></table></div></div>"""
            self.assertEqual(html, expected_html)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_alimento_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            nombre_alimento = "apple"
            response = requests.get(
                "http://{}:{}/alimento/{}".format(IP, PORT, nombre_alimento)
            )

            self.assertEqual(response.status_code, 200)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_alimento_contenido_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            nombre_alimento = "apple"
            response = requests.get(
                "http://{}:{}/alimento/{}".format(IP, PORT, nombre_alimento)
            )

            self.assertIn(
                "<tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>",
                response.text,
            )

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=jueves&tipo_comida=".format(IP, PORT)
            )
            html = response.text
            expected_html = """<link rel='stylesheet' type='text/css' href='styles.css'><table class='container'><tr><th>Desayuno</th><th>Comida</th><th>Cena</th></tr><tr><td>Pera</td><td>Cebolla</td><td>Pollo</td></tr></table>"""
            self.assertEqual(html, expected_html)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_status_code_get_(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=jueves&tipo_comida=".format(IP, PORT)
            )

            self.assertEqual(response.status_code, 200)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_contenido_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=jueves&tipo_comida=".format(IP, PORT)
            )

            self.assertIn(
                "<tr><th>Desayuno</th><th>Comida</th><th>Cena</th></tr>",
                response.text,
            )

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_error_dia_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=juevesj&tipo_comida=".format(IP, PORT)
            )
            html = response.text
            expected_html = """Día no válido."""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_error_dia_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=juevesj&tipo_comida=".format(IP, PORT)
            )

            self.assertEqual(response.status_code, 404)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_error_dia_no_especificados_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=&tipo_comida=".format(IP, PORT)
            )
            html = response.text
            expected_html = """Día y comida no especificados."""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_error_dia_no_especificados_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=&tipo_comida=".format(IP, PORT)
            )

            self.assertEqual(response.status_code, 404)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_error_comida_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=jueves&tipo_comida=ds".format(
                    IP, PORT
                )
            )
            html = response.text
            expected_html = """Comida no válida."""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_consulta_error_comida_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=jueves&tipo_comida=ds".format(
                    IP, PORT
                )
            )

            self.assertEqual(response.status_code, 404)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_logo_get(self):
        # Inicia el servidor en un hilo separado
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        # Espera un poco para asegurarse de que el servidor esté activo
        time.sleep(1)

        try:
            # Realiza una solicitud GET al endpoint que sirve el archivo de imagen del logo
            response = requests.get(f"http://{IP}:{PORT}/logoURJC.png")

            # Verifica que la respuesta tenga el código de estado 200

            # Carga el contenido esperado del archivo logoURJC.png
            with open("logoURJC.png", "rb") as f:
                expected_image = f.read()

            # Verifica que el contenido de la respuesta coincida con el contenido del archivo de imagen
            self.assertEqual(
                response.content, expected_image, "Image content does not match"
            )
        finally:
            # Apaga el servidor
            server.shutdown()
            server.server_close()
            server_thread.join()

    def test_request_logo__statud_code_get(self):
        # Inicia el servidor en un hilo separado
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        # Espera un poco para asegurarse de que el servidor esté activo
        time.sleep(1)

        try:
            # Realiza una solicitud GET al endpoint que sirve el archivo de imagen del logo
            response = requests.get(f"http://{IP}:{PORT}/logoURJC.png")

            # Verifica que la respuesta tenga el código de estado 200
            self.assertEqual(
                response.status_code,
                200,
                f"Unexpected status code: {response.status_code}",
            )

        finally:
            # Apaga el servidor
            server.shutdown()
            server.server_close()
            server_thread.join()

    def test_request_logo_cabecera_get(self):
        # Inicia el servidor en un hilo separado
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        # Espera un poco para asegurarse de que el servidor esté activo
        time.sleep(1)

        try:
            # Realiza una solicitud GET al endpoint que sirve el archivo de imagen del logo
            response = requests.get(f"http://{IP}:{PORT}/logoURJC.png")

            # Verifica que la respuesta tenga el código de estado 200

            # Carga el contenido esperado del archivo logoURJC.png
            with open("logoURJC.png", "rb") as f:
                expected_image = f.read()

            # Verifica que el contenido de la respuesta coincida con el contenido del archivo de imagen
            self.assertEqual(
                response.content, expected_image, "Image content does not match"
            )
        finally:
            # Apaga el servidor
            server.shutdown()
            server.server_close()
            server_thread.join()

    def test_handle_styles_request_status_code_get(self):
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        time.sleep(1)

        try:
            response = requests.get(f"http://{IP}:{PORT}/styles.css")

            self.assertEqual(response.status_code, 200)

        finally:
            server.shutdown()
            server.server_close()
            server_thread.join()

    def test_handle_styles_request_cabecera_get(self):
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        time.sleep(1)

        try:
            response = requests.get(f"http://{IP}:{PORT}/styles.css")

            self.assertEqual(
                response.headers["Content-Type"],
                "text/css; charset=utf-8",
                "Unexpected content type",
            )

        finally:
            server.shutdown()
            server.server_close()
            server_thread.join()

    def test_handle_index_request_get(self):
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        time.sleep(1)

        try:
            response = requests.get("http://{}:{}/index.html".format(IP, PORT))
            html = response.text
            expected_html = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Formulario Registro</title>
    
    <script>
        function mostrarCarga(formId) {
            document.getElementById("loader").style.display = "block";
            setTimeout(function() {
                // Envía el formulario después del retraso
                document.getElementById(formId).submit(); 
            }, 1000); // Envía el formulario después de 3 segundos

            // Retorna false para evitar que el formulario se envíe automáticamente
            return false;
        }
         function mostrarCarga2(formId) {
            document.getElementById("loader2").style.display = "block";
            setTimeout(function() {
                // Envía el formulario después del retraso
                document.getElementById(formId).submit(); 
            }, 1000); // Envía el formulario después de 3 segundos

            // Retorna false para evitar que el formulario se envíe automáticamente
            return false;
        }

        function girarFormulario() {
            var form = document.querySelector('.form-register');
            form.classList.toggle('rotated');
            
            // Cambia los campos del formulario original después de la animación de giro
            setTimeout(function() {
                if (form.classList.contains('rotated')) {
                    // Muestra el segundo formulario
                    document.getElementById('formOriginal').style.display = 'none';
                    document.getElementById('formAdicional').style.display = 'block';
                    // Cambia el texto del título del formulario
                    document.querySelector('#formAdicional h4').textContent = 'Formulario Adicional 🥗';
                } else {
                    // Muestra el primer formulario
                    document.getElementById('formOriginal').style.display = 'block';
                    document.getElementById('formAdicional').style.display = 'none';
                    // Cambia el texto del título del formulario
                    document.querySelector('#formOriginal h4').textContent = 'Formulario Registro 🥗';
                }
            }, 1000); // Espera 1 segundo para la animación de giro
        }

        // Detectar cuándo la página se muestra nuevamente desde el caché
        window.addEventListener('pageshow', function(event) {
            // Si la página se muestra nuevamente desde el caché, ocultar el indicador de carga
            document.getElementById("loader").style.display = "none";
                        document.getElementById("loader2").style.display = "none";

        }
       
        );
    </script>
</head>
<body>
    <section class="form-register">
        <div id="formOriginal">
            <h4 onclick="girarFormulario()">Formulario Registro 🥗</h4>
            <form action="/consulta" method="get" id="myForm">
                <input class="controls" type="text" name="dia_semana" placeholder="Día de la semana">
                <input class="controls" type="text" name="tipo_comida" placeholder="Comida que quiero consultar (opcional)">
                <p id="resultado">Cargado en <a href="#" id="cambiarContenido">JSON clíck aquí para cambiar a XML</a> </p>              
                  <input class="botons" type="button" value="Consultar" onclick="mostrarCarga('myForm')">
                <p><a href="#">¿Ya tengo Cuenta?</a></p>
            </form>
            <div id="loader" class="loader" style="display: none;"></div>
        </div>
        <div id="formAdicional" style="display: none;">
            <h4 onclick="girarFormulario()">Formulario Registro 🥗</h4>
           <form action="/agregar_alimento" method="post" id="addForm">
        <input class="controls" type="text" name="nombre_alimento" placeholder="Nombre del alimento">
        <input class="controls" type="number" name="proteinas" placeholder="Proteínas">
        <input class="controls" type="number" name="carbohidratos" placeholder="Carbohidratos">
        <input class="controls" type="number" name="grasas" placeholder="Grasas">
        <input class="controls" type="number" name="fibra" placeholder="Fibra">
        <input class="controls" type="number" name="colesterol" placeholder="Colesterol">
        <select class="controls" name="categoria">
            <option value="FRUTAS">Frutas</option>
            <option value="CARNES">Carnes</option>
            <option value="VERDURAS">Verduras</option>
        </select>
        <input class="botons" type="button" value="Agregar Alimento" onclick="agregarAlimento()">
    </form>
  
    <!DOCTYPE html>

  <script>
        function agregarAlimento() {
            document.getElementById("loader2").style.display = "block";
            setTimeout(function() {
                document.getElementById("addForm").submit();
            }, 1000);
            return false;
        }

        const cambiarContenido = document.getElementById('cambiarContenido');

// Agregar un event listener al enlace
cambiarContenido.addEventListener('click', function() {
    var enlace = document.getElementById("cambiarContenido");
    var form = document.getElementById("myForm");

    if (enlace.innerText === "JSON clíck aquí para cambiar a XML") {
        enlace.innerText = "XML clíck aquí para cambiar a JSON";
        form.action = "/XML";
    } else {
        enlace.innerText = "JSON clíck aquí para cambiar a XML";
        form.action = "/consulta";
    }
});
    </script>
            <div id="loader2" class="loader" style="display: none;"></div>
        </div>
    </section>
    <a href="/autor"> <img src="logoURJC.png" href="/autor" alt="Logo" class="logo"></a>
</body>
</html><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <title>Once</title>
  <style>
    .container2 {
      position: fixed;
      bottom: 10px;
      right: 40px;
      height: auto;
    }

    .container2 {
      display: flex;
      align-items: center;
    }

    .w {
      width: 0px;
      margin: 0;
      outline: none;
      box-sizing: border-box;
      height: 50px;
      padding: 0 20px;
      color: #000;
      border-radius: 50px;
      font-size: 20px;
      transition: all .7s ease;
      box-shadow: none;
      color: white;
      background-color: rgba(131, 166, 146, 0.5);
      border: 2px solid rgba(131, 166, 146, 0.5);
    }

    ::placeholder {
      color: white;
    }

    .btn {
      width: 70px;
      height: 70px;
      background: #4592a0;
      border-radius: 50%;
      text-align: center;
      cursor: pointer;
      transition: .5s;
      margin-left: -70px;
      display: flex;
      align-items: center;
      justify-content: center;
      border: 1px solid #4592a0;
      font-size: 50px;
    }

    .btn i {
      font-size: 25px;
      transition: all .7s ease;
      line-height: 80px;
    }

    .container2:hover .btn {
      transform: rotate(-360deg);
    }

    .btn:hover {
      background: #83a692;
    }

    .container2:hover input {
      width: 279px;
    }
  

  </style>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="envuelve">
  <div class="wrapper">
    <div class="container2">
      <input class="w" type="text" placeholder="Buscador" onkeypress="handleKeyPress(event)"/>
      <div class="btn">
        🥗
      </div>
    </div>
  </div>
</div>

<script>
  function handleKeyPress(event) {
    if (event.key === 'Enter') {
   
      window.location.href = 'http://localhost:21500/consulta/v2/' + document.querySelector('.w').value.toLowerCase();
    }
  }
  
</script>

</body>
</html>
"""
            self.assertEqual(html, expected_html)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_handle_index_request_status_code_get(self):
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        time.sleep(1)

        try:
            response = requests.get("http://{}:{}/index.html".format(IP, PORT))
            html = response.text

            self.assertEqual(response.status_code, 200)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_encontrado_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/consulta/v2/apple".format(IP, PORT))
            html = response.text
            print(html)
            expected_html = """<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'><div style='display:flex;'><div style='margin-right: 20px;'><p class='pp'> Fichero </p><table class='container'><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.3</td></tr><tr><td>Carbohidratos</td><td>25</td></tr><tr><td>Grasas</td><td>0.4</td></tr><tr><td>Fibra</td><td>4.4</td></tr><tr><td>Colesterol</td><td>0</td></tr></table></div><div><table class='container'><p class='pp' > API </p><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.0</td></tr><tr><td>Carbohidratos</td><td>14.3</td></tr><tr><td>Grasas</td><td>0.65</td></tr><tr><td>Fibra</td><td>3.2</td></tr><tr><td>Colesterol</td><td>0.0</td></tr></table></div></div>"""
            self.assertEqual(html, expected_html)
            self.assertEqual(response.status_code, 200)
            self.assertIn(
                "<tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.3</td></tr><tr><td>Carbohidratos</td>",
                response.text,
            )
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_encontrado_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/consulta/v2/apple".format(IP, PORT))
            html = response.text
            expected_html = """<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'><div style='display:flex;'><div style='margin-right: 20px;'><p class='pp'> Fichero </p><table class='container'><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.3</td></tr><tr><td>Carbohidratos</td><td>25</td></tr><tr><td>Grasas</td><td>0.4</td></tr><tr><td>Fibra</td><td>4.4</td></tr><tr><td>Colesterol</td><td>0</td></tr></table></div><div><table class='container'><p class='pp' > API </p><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.0</td></tr><tr><td>Carbohidratos</td><td>14.3</td></tr><tr><td>Grasas</td><td>0.65</td></tr><tr><td>Fibra</td><td>3.2</td></tr><tr><td>Colesterol</td><td>0.0</td></tr></table></div></div>"""
            self.assertEqual(response.status_code, 200)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_encontrado_contenido_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/consulta/v2/apple".format(IP, PORT))

            self.assertIn(
                "<tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.3</td></tr><tr><td>Carbohidratos</td>",
                response.text,
            )
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_error_api_fichero_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta/v2/34848575".format(IP, PORT)
            )
            html = response.text
            expected_html = """<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'><p>No se encontraron nutrientes para el alimento especificado.</p>"""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_error_api_fichero_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta/v2/34848575".format(IP, PORT)
            )

            self.assertEqual(response.status_code, 200)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_error_fichero_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/consulta/v2/manzana".format(IP, PORT))
            html = response.text
            expected_html = """<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'><div style='display:flex;'><div style='margin-right: 20px;'><p class='pp'> Fichero </p><p>No se encontraron nutrientes locales para el alimento especificado.</p></div><div><table class='container'><p class='pp' > API </p><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>0.0</td></tr><tr><td>Carbohidratos</td><td>14.4</td></tr><tr><td>Grasas</td><td>0.0</td></tr><tr><td>Fibra</td><td>None</td></tr><tr><td>Colesterol</td><td>None</td></tr></table></div></div>"""
            self.assertEqual(html, expected_html)
            self.assertIn(
                "<link rel='stylesheet' type='text/css' href='styles.css'><div style='display:flex;'>",
                response.text,
            )
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_error_fichero_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/consulta/v2/manzana".format(IP, PORT))

            self.assertEqual(response.status_code, 200)

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_V2_error_api_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/consulta/v2/martrdd".format(IP, PORT))
            html = response.text
            expected_html = """<div class='a'> <link rel='stylesheet' type='text/css' href='styles.css'><div style='display:flex;'><div style='margin-right: 20px;'><p class='pp'> Fichero </p><table class='container'><tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>234.0</td></tr><tr><td>Carbohidratos</td><td>333.0</td></tr><tr><td>Grasas</td><td>333.0</td></tr><tr><td>Fibra</td><td>333.0</td></tr><tr><td>Colesterol</td><td>333.0</td></tr></table></div><div><p>No se encontraron nutrientes para el alimento en la API.</p></div></div>"""
            self.assertEqual(html, expected_html)
            self.assertEqual(response.status_code, 200)
            self.assertIn(
                "<tr><th>Nutriente</th><th>Valor</th></tr><tr><td>Proteinas</td><td>234.0</td></tr><tr><td>Carbohidratos</td>",
                response.text,
            )
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_XML_request_error_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/XML?dia_semana=lunejs&tipo_comida=dejsayuno".format(
                    IP, PORT
                )
            )
            html = response.text
            expected_html = """<link rel='stylesheet' type='text/css' href='styles.css'><table class='container'><tr><th>Desayuno</th><th>Comida</th><th>Cena</th></tr></table>"""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_XML_request_error_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/XML?dia_semana=lunejs&tipo_comida=dejsayuno".format(
                    IP, PORT
                )
            )

            self.assertEqual(response.status_code, 200)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_XML_request_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/XML?dia_semana=lunes&tipo_comida=desayuno".format(
                    IP, PORT
                )
            )
            html = response.text
            expected_html = """<link rel='stylesheet' type='text/css' href='styles.css'><table class='container'><tr><th>Desayuno</th></tr><tr><td>Manzana</td></tr></table>"""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_XML_request_get_status_code(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/XML?dia_semana=lunes&tipo_comida=desayuno".format(
                    IP, PORT
                )
            )

            self.assertEqual(response.status_code, 200)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_prueba_request_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/prueba".format(IP, PORT))
            html = response.text

            expected_html = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Formulario Registro</title>
    
    <script>
        function mostrarCarga(formId) {
            document.getElementById("loader").style.display = "block";
            setTimeout(function() {
                // Envía el formulario después del retraso
                document.getElementById(formId).submit(); 
            }, 1000); // Envía el formulario después de 3 segundos

            // Retorna false para evitar que el formulario se envíe automáticamente
            return false;
        }
         function mostrarCarga2(formId) {
            document.getElementById("loader2").style.display = "block";
            setTimeout(function() {
                // Envía el formulario después del retraso
                document.getElementById(formId).submit(); 
            }, 1000); // Envía el formulario después de 3 segundos

            // Retorna false para evitar que el formulario se envíe automáticamente
            return false;
        }

        function girarFormulario() {
            var form = document.querySelector('.form-register');
            form.classList.toggle('rotated');
            
            // Cambia los campos del formulario original después de la animación de giro
            setTimeout(function() {
                if (form.classList.contains('rotated')) {
                    // Muestra el segundo formulario
                    document.getElementById('formOriginal').style.display = 'none';
                    document.getElementById('formAdicional').style.display = 'block';
                    // Cambia el texto del título del formulario
                    document.querySelector('#formAdicional h4').textContent = 'Formulario Adicional 🥗';
                } else {
                    // Muestra el primer formulario
                    document.getElementById('formOriginal').style.display = 'block';
                    document.getElementById('formAdicional').style.display = 'none';
                    // Cambia el texto del título del formulario
                    document.querySelector('#formOriginal h4').textContent = 'Formulario Registro 🥗';
                }
            }, 1000); // Espera 1 segundo para la animación de giro
        }

        // Detectar cuándo la página se muestra nuevamente desde el caché
        window.addEventListener('pageshow', function(event) {
            // Si la página se muestra nuevamente desde el caché, ocultar el indicador de carga
            document.getElementById("loader").style.display = "none";
                        document.getElementById("loader2").style.display = "none";

        }
       
        );
    </script>
</head>
<body>
    <section class="form-register">
        <div id="formOriginal">
            <h4>Agregar un alimento a una semana y una comida</h4>
           
            <form action="/agregar_alimento2" method="post" id="addForm">
                <input class="controls" type="text" name="nombre_alimento" placeholder="Nombre del alimento">
            
            <select class="controls" name="día">
                <option value="lunes">Lunes</option>
                <option value="martes">Martes</option>
                <option value="miercoles">Miercoles</option>
                <option value="jueves">Jueves</option>
                <option value="viernes">Viernes</option>
                <option value="sabado">Sabado</option>
                <option value="domingo">Domingo</option>
            
            </select>
            <select class="controls" name="comida">
                <option value="desayuno">desayuno</option>
                <option value="comida">comida</option>
                <option value="cena">cena</option>
               
            
            </select>
        <input class="botons" type="button" value="Agregar Alimento" onclick="agregarAlimento()">
    </form>
  
    <!DOCTYPE html>

  <script>
        function agregarAlimento() {
            document.getElementById("loader2").style.display = "block";
            setTimeout(function() {
                document.getElementById("addForm").submit();
            }, 1000);
            return false;
        }

        const cambiarContenido = document.getElementById('cambiarContenido');

// Agregar un event listener al enlace
cambiarContenido.addEventListener('click', function() {
    var enlace = document.getElementById("cambiarContenido");
    var form = document.getElementById("myForm");

    if (enlace.innerText === "JSON clíck aquí para cambiar a XML") {
        enlace.innerText = "XML clíck aquí para cambiar a JSON";
        form.action = "/XML";
    } else {
        enlace.innerText = "JSON clíck aquí para cambiar a XML";
        form.action = "/consulta";
    }
});
    </script>
            <div id="loader2" class="loader" style="display: none;"></div>
        </div>
    </section>
    <a href="/autor"> <img src="logoURJC.png" href="/autor" alt="Logo" class="logo"></a>
</body>
</html>

"""
            self.assertEqual(html, expected_html)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_prueba_request_status_code_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get("http://{}:{}/prueba".format(IP, PORT))

            self.assertEqual(response.status_code, 200)
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_not_found(self):
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()

        # Wait a bit for the server to come up
        time.sleep(1)

        try:
            response = requests.get("http://{}:{}/unknown".format(IP, PORT))
            self.assertEqual(response.status_code, 404)
            self.assertIn("Página no encontrada.", response.text)
        finally:
            # Shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_favicon(self):
        IP = "127.0.0.1"  # Dirección IP del servidor
        PORT = 21500  # Puerto en el que el servidor está escuchando

        # Crear una instancia del servidor HTTP
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()

        # Esperar un poco para que el servidor se inicie
        time.sleep(1)

        try:
            # Realizar la solicitud GET al favicon.ico
            response = requests.get("http://{}:{}/favicon.ico".format(IP, PORT))

            # Verificar si la respuesta es exitosa (código 200)
            self.assertEqual(response.status_code, 200)
        finally:
            # Detener el servidor después de la prueba
            server.shutdown()
            server.server_close()

    def test_response_time(self):

        # Crear una instancia del servidor HTTP
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        try:
            response = requests.get("http://{}:{}/".format(IP, PORT))
            # Aquí puedes agregar tu propia lógica para verificar si el tiempo de respuesta es aceptable
            self.assertLessEqual(
                response.elapsed.total_seconds(), 1.0
            )  # Tiempo máximo esperado en segundos
        finally:
            # Detener el servidor después de la prueba
            server.shutdown()
            server.server_close()

    def test_request_consulta_style_get(self):
        # start our server
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        # Wait a bit for the server to come up
        time.sleep(1)
        try:
            response = requests.get(
                "http://{}:{}/consulta?dia_semana=jueves&tipo_comida=".format(IP, PORT)
            )

            self.assertEqual(response.status_code, 200)
            self.assertIn(
                "<link rel='stylesheet' type='text/css' href='styles.css'>",
                response.text,
            )

        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_handle_loader_request_get(self):
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()
        time.sleep(1)

        try:
            response = requests.get("http://{}:{}/index.html".format(IP, PORT))

            self.assertEqual(response.status_code, 200)
            self.assertIn(
                'div id="loader" class="loader" style="display: none;"></div>',
                response.text,
            )
        finally:
            # and shutdown the server
            server.shutdown()
            server.server_close()

    def test_request_imagen_favicon(self):
        IP = "127.0.0.1"  # Dirección IP del servidor
        PORT = 21500  # Puerto en el que el servidor está escuchando

        # Crear una instancia del servidor HTTP
        server = HTTPServer((IP, PORT), ServidorNutricion)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        server_thread.start()

        # Esperar un poco para que el servidor se inicie
        time.sleep(1)

        try:
            # Realizar la solicitud GET al favicon.ico
            response = requests.get("http://{}:{}/favicon.ico".format(IP, PORT))

            # Verificar si la respuesta es exitosa (código 200)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response.headers["Content-Type"],
                "image/x-icon",
                "Unexpected content type",
            )
            self.assertEqual(
                response.headers["Content-Type"],
                "image/x-icon",
                "Unexpected content type",
            )
        finally:
            # Detener el servidor después de la prueba
            server.shutdown()
            server.server_close()


if __name__ == "__main__":
    unittest.main()
