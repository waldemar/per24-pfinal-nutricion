import unittest
from unittest.mock import patch
import json
from Nutricionista import Nutricionista


class TestNutricionista(unittest.TestCase):

    def setUp(self):
        with open("n_test.json", "w") as test_file:
            json.dump(
                {
                    "alimentos": [
                        {
                            "nombre": "Manzana",
                            "nutrientes": {
                                "vitaminas": ["A", "C"],
                                "minerales": ["potasio", "calcio"],
                            },
                        }
                    ],
                    "dietas": [
                        {
                            "menu_semanal": {
                                "lunes": {
                                    "desayuno": ["Manzana"],
                                    "comida": [],
                                    "cena": [],
                                }
                            }
                        }
                    ],
                },
                test_file,
            )

        self.nutricionista = Nutricionista("n_test.json")

    def tearDown(self):
        pass

    def test_obtener_nutrientes_alimento_existente(self):
        nutrientes = self.nutricionista.obtener_nutrientes_alimento("Manzana")
        self.assertIsNotNone(nutrientes)
        self.assertEqual(
            nutrientes, {"vitaminas": ["A", "C"], "minerales": ["potasio", "calcio"]}
        )

    def test_obtener_nutrientes_alimento_inexistente(self):
        nutrientes = self.nutricionista.obtener_nutrientes_alimento("Pera")
        self.assertIsNone(nutrientes)

    def test_existe_alimento_existente(self):
        existe = self.nutricionista.existe_alimento("Manzana")
        self.assertTrue(existe)

    def test_existe_alimento_inexistente(self):
        existe = self.nutricionista.existe_alimento("Pera")
        self.assertFalse(existe)


if __name__ == "__main__":
    unittest.main()
