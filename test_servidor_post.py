import threading
import time
import unittest
import requests
from http.server import HTTPServer
from servidor import ServidorNutricion

IP = "127.0.0.1"
PORT = 21500


class TestRequests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = HTTPServer((IP, PORT), ServidorNutricion)
        cls.server_thread = threading.Thread(target=cls.server.serve_forever)
        cls.server_thread.start()
        time.sleep(1)

    @classmethod
    def tearDownClass(cls):
        cls.server.shutdown()
        cls.server.server_close()
        cls.server_thread.join()

    def test_add_food_success(self):
        myobj = {
            "nombre_alimento": "hhhjd",
            "proteinas": "0.3",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "0",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertIn("Nuevo alimento agregado", response.text)

    def test_add_food_success_status_code(self):
        myobj = {
            "nombre_alimento": "ddsswdsdsds",
            "proteinas": "0.3",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "0",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertEqual(response.status_code, 200)

    def test_add_food_success_exists_status_code(self):
        myobj = {
            "nombre_alimento": "ekeke",
            "proteinas": "0.3",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "0",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertEqual(response.status_code, 400)

    def test_add_food_success_existss_status_code(self):
        myobj = {
            "nombre_alimento": "ekeke",
            "proteinas": "0.3",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "0",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertIn(
            "Ya existe este alimento",
            response.text,
        )

    def test_add_food_missing_name(self):
        myobj = {
            "proteinas": "0.3",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "j",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertIn("Debe proporcionar el nombre del alimento", response.text)

    def test_add_food_missing_name_status_code(self):
        myobj = {
            "proteinas": "0.3",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "j",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertEqual(response.status_code, 400)

    def test_invalid_route(self):
        myobj = {"somekey": "somevalue"}
        response = requests.post(f"http://{IP}:{PORT}/invalid_route", data=myobj)
        self.assertEqual(response.status_code, 404)

    def test_invalid_route_status_code(self):
        myobj = {"somekey": "somevalue"}
        response = requests.post(f"http://{IP}:{PORT}/invalid_route", data=myobj)
        self.assertEqual(response.status_code, 404)

    def test_add_food_invalid_nutrient(self):
        myobj = {
            "nombre_alimento": "67965",
            "proteinas": "abc",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "0",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertIn("Debes corregir el formulario", response.text)

    def test_add_food_invalid_nutrient_status_code(self):
        myobj = {
            "nombre_alimento": "67965",
            "proteinas": "abc",
            "carbohidratos": "14",
            "grasas": "0.2",
            "fibra": "2.4",
            "colesterol": "0",
            "categoria": "Fruta",
        }
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento", data=myobj)
        self.assertEqual(response.status_code, 400)

    def test_add_food_to_meal_success(self):
        myobj = {"nombre_alimento": "Pollo", "día": "Lunes", "comida": "Desayuno"}
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento2", data=myobj)
        self.assertIn("Nuevo alimento agregado", response.text)

    def test_add_food_to_meal_success_status_code(self):
        myobj = {"nombre_alimento": "Pollo", "día": "Lunes", "comida": "Desayuno"}
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento2", data=myobj)
        self.assertEqual(response.status_code, 200)

    def test_add_food_to_meal_missing_name(self):
        myobj = {"día": "Lunes", "comida": "Desayuno"}
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento2", data=myobj)
        self.assertIn("Debe proporcionar el nombre del alimento", response.text)

    def test_add_food_to_meal_missing_name_status_code(self):
        myobj = {"día": "Lunes", "comida": "Desayuno"}
        response = requests.post(f"http://{IP}:{PORT}/agregar_alimento2", data=myobj)
        self.assertEqual(response.status_code, 400)


if __name__ == "__main__":
    unittest.main()

